﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Intergation;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Intergation
{

    public class IntegrationEvents : IIntegrationEvents
    {
        private readonly IServiceScopeFactory _scopeFactory;
       
        public IntegrationEvents(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task UpdatePromoCodesEvent(string notificationMessage)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var _employeeRepositoty = scope.ServiceProvider.GetRequiredService<IRepository<Employee>>();

                var adminNotified = JsonSerializer.Deserialize<NotifyAdminAboutPartnerManagerPromoCodeMsg>(notificationMessage);
                Guid adminId;

                if (adminNotified != null && adminNotified.PartnerManagerId != null)
                    adminId = (Guid)adminNotified.PartnerManagerId;
                else
                    return;

                try
                {
                    var employee = await _employeeRepositoty.GetByIdAsync(adminId);
                    employee.AppliedPromocodesCount++;
                    await _employeeRepositoty.UpdateAsync(employee);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"--> Could not update PromoCodes: {e.Message}");
                    throw;
                }
            }
        }
    }
}
