﻿using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Intergation
{

    public interface IIntegrationEvents 
    {
        Task UpdatePromoCodesEvent(string notificationMessage);
    }
}
