﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Intergation
{

    public interface IIntegrationEvents 
    {
        Task GivePromoCodeToCustomer(string notificationMessage);
    }
}
