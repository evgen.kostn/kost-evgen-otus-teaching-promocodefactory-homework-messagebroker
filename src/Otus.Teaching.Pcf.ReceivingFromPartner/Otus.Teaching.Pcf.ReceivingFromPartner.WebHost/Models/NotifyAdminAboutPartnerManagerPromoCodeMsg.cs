﻿using System;
namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models
{
    public class NotifyAdminAboutPartnerManagerPromoCodeMsg
    {
        public Guid? PartnerManagerId { get; set; }
    }
}

